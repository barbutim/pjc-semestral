Po inverzním počasí, které bude panovat v Česku v tomto týdnu, se výrazně ochladí po 5. prosinci. Noční teploty by mohly klesnou až k minus osmi stupňům Celsia, průměr by se měl držet kolem minus tří stupňů Celsia. Denní teploty by se měly držet jen lehce nad nulou, ale mohou klesnout i k minus pěti stupňům.


Foto: Archiv čtenářky
Ilustrační foto

Článek
Nejchladněji by mělo být v týdnu od 12. do 18. prosince, uvádí měsíční výhled počasí Českého hydrometeorologického ústavu.

Počasí v tomto týdnu bude mít v Česku převážně inverzní charakter. Denní teploty se budou většinou držet nad nulou, v noci je ale třeba počítat s mrazem. Při zmenšené oblačnosti mohou teploty klesnout až k minus pěti stupňům Celsia. Sluníčka si také příliš neužijeme. Provázet nás budou mlhy, mrholení, místy i mrznoucí, a případně slabé sněžení.

Mikulášskou nadílku bude provázet ochlazení. Mělo by být polojasno až oblačno, místy by mělo sněžit, v nižších polohách by mělo pršet. Noční teploty se budou pohybovat kolem průměrných minus tří stupňů, místy klesnou až na minus osm stupňů. Denní teploty budou oscilovat mezi -5 a 5 °C.

Zřejmě nejchladnějším týdnem ve sledovaném období by se podle měsíčního výhledu mělo stát období od 12. do 18. prosince. Meteorologové počítají s nočními mrazy kolem průměrných minus čtyř stupňů, na některých místech mohou klesnout až na minus devět stupů. Denní teploty by se měly pohybovat kolem nuly, ale mohou klesnout až na minus osm stupňů.

V týdnu před vánočními svátky se mírně oteplí, vyplývá z předpovědi. Od 19. do 25. prosince by se měly noční teploty pohybovat kolem minus tří stupňů a denní se budou pohybovat na průměrném jednom stupni Celsia.

Celkově budou následující čtyři týdny teplotně i srážkově průměrné až slabě podprůměrné, uvádí Český hydrometeorologický ústav. Srážky budou mít podle meteorologů převážně podobu mrholení, ojediněle i mrznoucího, nebo slabého sněžení.

Asi šest a půl kilometrů jel po slovenské dálnici D1 ve směru z Košic do Prešova řidič osobního vozu. Když ho zastavili policisté, prohlásil, že se domníval, že silnice je obousměrná.


Foto: Policie SR
Řidič v protisměru na slovenské dálnici

Článek
„V sobotu dopoledne jsme dostali oznámení o automobilu v protisměru na dálnici mezi Košicemi a Prešovem. Policisté auto zastavili za dálničním odpočívadlem,“ uvedla v pondělí policie na sociální síti.

Řidič policistům řekl, že na dálnici sjel z mostu na křižovatce u obce Lemešany a chtěl se dostat do Prešova. „Po dálnici jel v protisměru asi 6,5 kilometru,“ dodala policie. Řidiči byla uložena bloková pokuta, ale její výši policisté neupřesnili.

Ostatní řidiči, kteří viděli vůz jedoucí po dálnici v protisměru, mluví o velkém štěstí, že nedošlo k vážné nehodě. „Upozorňovali jsme ho blikáním světel, ale on jel dál s klidem Angličana,“ divila se Marta. „Má štěstí, že přežil a nikdo nevinný nedoplatil na jeho hloupost,“ uvedla v diskusi na sociální síti Agátha. „Bloková pokuta??? V Německu se za jízdu po dálnici v protisměru odebere řidičák doživotně,“ napsal Martin.

Spor mezi americkým podnikatelem Elonem Muskem a společností Apple v pondělí nečekaně vygradoval. Stalo se tak poté, co výrobce iPhonů stáhnul z Twitteru veškerou reklamu. Nejbohatší muž na světě kontroval tím, že mu vadí poplatky za aplikace. Kvůli tomu se podle vlastního vyjádření chystá na „válku s Applem“.


Foto: Mike Blake, Reuters
Elon Musk

Článek
Musk se stal ředitelem Twitteru na konci října, bezprostředně poté, co jej koupil. Okamžitě ale začal dělat nepopulární změny, kdy propustil celé vedení a polovinu ze 7500 zaměstnanců – údajně kvůli neuspokojivým finančním výsledkům.

Na zbylé zaměstnance začal tak moc tlačit, že stovky dalších raději daly výpověď. Důsledky na sebe nenechaly dlouho čekat. Za měsíc od převzetí Twitteru Muskem se na této platformě výrazně zvýšil počet rasistických, antisemitských a jinak nenávistných projevů, uvedly organizace, které síť monitorují. Musk tvrdí, že na sociální síti prosazuje absolutní svobodu.

Inzerci kvůli tomu omezily velké společnosti, jako je výrobce léků Pfizer, automobilka General Motors či koncern Volkswagen. A zároveň i Apple, jak dal Musk v pondělí nespokojeně najevo: „Apple prakticky přestal inzerovat na Twitteru. To tak nenávidí svobodu slova v Americe?“

Twitter může zkolabovat
Internet a PC

Během pouhé hodiny pak sdílel hned několik příspěvků, ve kterých se navážel do Applu kvůli tomu, že si účtuje až 30% poplatky z uskutečněných transakcí ze všech aplikací, které si lidé stáhnou a používají je. Označil to za „skrytou daň na internetu“.

Čisté zuby se nekazí: Domácí péče o zuby je stěžejní

Završil to v poslední době populárním memem s driftujícím autem na dálnici, ve kterém jasně dává najevo, že si 30% poplatky nenechá líbit a půjde s Applem do války.


Musk dal jasně najevo, jaké má plány.

Otázka poplatků je přitom pro Muska více než ožehavá. Podle jeho dřívějších prohlášení totiž Twitter ztrácí denně čtyři miliony dolarů (94 milionů Kč), což je dlouhodobě neudržitelná situace. Nejbohatší muž na světě věří, že se mu podaří dostat z červených čísel právě díky předplatnému pro ověřené účty, které by měl na konci tohoto týdne spustit.

„Zlaté ověření pro společnosti, šedé pro vlády, modré pro jednotlivce (ať jsou celebrity, nebo ne),“ napsal Musk. Předplatné by mělo činit osm dolarů (asi 200 Kč) za jeden účet. Pokud by si svůj díl ukousla společnost Apple, Twitter by z této sumy dostal jen 5,3 dolaru (133 korun).

Twitter v reakci na Muskovo ultimátum opouštějí stovky zaměstnanců
Internet a PC

Vyrobí konkurenci pro iPhone?
Ve svém tažení je přitom novopečený šéf Twitteru podle všeho velmi odhodlaný. „Upřímně doufám, že to nezajde až tak daleko… Ale ano, pokud nebude jiná možnost, vyrobím klidně konkurenční telefon (pro Google a Apple),“ prohlásil Musk.

V řadě příspěvků na Twitteru totiž vedle Applu zmiňoval také americký Google, který si na Androidu účtuje obdobné poplatky.

Google ale takový problém pro Muska nepředstavuje. Americký podnik totiž umožňuje na Androidu relativně snadno instalovat aplikace z alternativních obchodů, díky čemuž nenutí uživatele používat jeho oficiální obchod, ve kterém si účtuje poplatky.

V případě platformy iOS od Applu to ale tak snadné není, uživatelé standardně nemohou alternativní obchody využívat. Jde to v podstatě jen po instalaci jailbreaku, který odemkne jinak omezené možnosti systému iOS. Tím ale lidé přijdou o záruku na svá zařízení.


Poplatky označuje Musk za „skrytou daň na internetu“.

Soud s tvůrci Fortnitu
Spor o poplatky otevřelo již v srpnu 2020 herní studio Epic Games. Ve hře Fortnite tehdy vedení studia nabídlo hráčům alternativní platební metodu, díky níž mohli nakupovat virtuální měnu V-Bucks, jež se používá k nákupům oblečků, tanečků a dalšího vybavení, o 20 procent levněji.

Na to okamžitě zareagovali oba technologičtí giganti a Fortnite ze svých obchodů App Store a Google Play stáhli. Na to evidentně vedení Epic Games čekalo a obratem podalo na obě společnosti žalobu za to, že mají na mobilním trhu monopol a že zneužívají své postavení.

Od té doby se rozhořelo napříč světem hned několik dalších sporů o poplatky, vstoupili do nich i antimonopolní regulátoři, kteří poukazují na možné omezování konkurence. V některých zemích už dokonce Apple začal ustupovat a snaží se dohodnout na nastavení jasných pravidel.

Německý soud vydal nový zatykač na muže, který je podezřelý, že před 15 lety stál za velmi sledovaným případem zmizení tříleté Madeleine McCannové na rodinné dovolené v Portugalsku. Pětačtyřicetiletý Christian Brückner je teď sice ve vězení, ale úřady se podle agentury AP obávají, aby se nedostal na svobodu, než bude moci být souzen v dalších obdobných případech.


Foto: ČTK/AP
Poslední fotografie pořízená před zmizením Madeleine

Článek
Tříletá britská holčička Madeleine zmizela v květnu 2007 z apartmánu v turistickém komplexu v Praia da Luz u jihoportugalského města Lagos v regionu Algarve. Její rodiče byli v tu dobu na večeři v blízké restauraci. Vyšetřovatelé předpokládali, že Madeleine někdo unesl, po určitou dobu byli mezi podezřelými dokonce rodiče holčičky, o jejíž osud se i díky kampani příbuzných zajímala média po celém světě.

Až v roce 2020 vyslovili němečtí vyšetřovatelé nahlas podezření, že za zmizením a nejspíš i smrtí Madeleine stojí Brückner. Muž, kterého odborníci považují za psychopata a manipulativního narcisistu, byl za sexuální zneužívání dětí odsouzen už v letech 1994 a 2016. Nyní si odpykává sedmiletý trest vězení za mučení a znásilnění 72leté Američanky v Praia da Luz v roce 2005.

Tento trest mu vyprší v roce 2025. Aby se ale v žádném případě nestalo, že se dostane na svobodu, obžalovalo ho v říjnu státní zastupitelství ve středoněmeckém Braunschweigu z dalších pěti sexuálních zločinů, které podle něj spáchal taktéž v Portugalsku mezi lety 2000 a 2017.

Na to soud v Braunschweigu reagoval vydáním zatykače. Zdůvodnil to tím, že existují pádné důvody se domnívat, že Brückner skutečně spáchal trojici znásilnění a dva případy zneužívání dětí. Zatykač ještě musí být schválen v Itálii, kde byl v roce 2018 Brückner zatčen, než ho úřady vydaly do Německa.

V případu Madeleine zatím Brückner, který jakýkoliv podíl na jejím zmizení odmítá, obžalován nebyl. Je ale stále vyšetřován.

Podnikatel Tomáš Březina se neodvolá proti rozhodnutí ministerstva vnitra, které ho nezaregistrovalo do lednových prezidentských voleb. Je ale přesvědčený o tom, že by s odvoláním uspěl. Podpisů měl podle svého mínění dost. Nechce ale zatížit Česko náklady na opakování prezidentské volby. V pondělí o tom informoval v tiskové zprávě.


Foto: Jan Handrejch, Právo
Tomáš Březina (archivní snímek)

Článek
Kandidáti museli doložit nominaci podporou minimálně deseti senátorů, 20 poslanců nebo 50 000 řadových občanů. Březinovi ministerstvo ze 65 911 podpisů uznalo 45 894.

Březina tvrdí, že ministerstvu předal více než 70 000 podpisů, sesbíral jich přitom ještě o zhruba deset tisíc více. „Po dosažení tohoto čísla jsme sběr zastavili pro dostatečnost počtu podpisů. Následně jsme provedli trojnásobnou administrativní kontrolu podpisů. Vyřadili jsme 10 000 podpisů, u většiny se jednalo jen o naši předběžnou opatrnost. Byli jsme si proto jisti naší téměř nulovou chybovostí,“ uvedl Březina.

Je podle něj vyloučené, aby chybovost činila v průměru 29 procent. Upozornil například na to, že kandidáti, které podpořila vládní koalice, měli chybovost desetkrát nižší. „Takový rozdíl je objektivně nemožný. Vládní kandidáti využívali naprosto stejných metod sběru podpisů,“ podotkl Březina. Upozornil, že nezaměstnával žádnou agenturu. Zároveň se ale domnívá, že žádný kandidát nemohl sesbírat 80 000 podpisů sám bez cizí pomoci.

Hilšer: Z volby neodstoupím. Senát mě v politice už vyškolil
Prezidentské volby

„Jsem přesvědčen, že bych s odvoláním, ve světle výše uvedených faktů, bezpečně uspěl a mohl pokračovat v kampani a usilovat o post prezidenta republiky. Bohužel s třítýdenní přestávkou, bohužel v horké části kampaně. Tedy s velkými negativními důsledky mojí dočasné absence,“ uvedl Březina. „Nehodlám zatížit Českou republiku obrovskými finančními i lidskými náklady na opakování prezidentské volby,“ dodal.

Tento světový úspěch českých vědců by čekal málokdo

Ministerstvo vnitra do prezidentských voleb pustilo devět z 21 kandidátů. Odmítlo 12 kandidátních listin pro nedostatky a jednu přihlášku pro její pozdní předložení. Podnikatelé Karel Diviš a Karel Janeček se chtějí proti vyřazení odvolat.

O Hrad se tak utkají bývalý premiér a předseda hnutí ANO Andrej Babiš, poslanec SPD Jaroslav Bašta, senátoři Pavel Fischer a Marek Hilšer, bývalá rektorka Danuše Nerudová, bývalý vysoký představitel armády Petr Pavel, prezidentka České asociace povinných Denisa Rohanová, odborový předák Josef Středula a bývalý rektor Tomáš Zima.

Bezpečnostní experti z antivirové společnosti Eset varovali před falešnými fakturami, které se v posledních týdnech šíří českým internetem jako lavina. Nejde přitom o cíl vydělat na důvěřivcích, prostřednictvím příloh v nevyžádaných e-mailech se šíří nebezpečný spyware.


Foto: Kacper Pempel, Reuters
Ilustrační foto

Článek
Špionážní viry se dlouhodobě šíří prostřednictvím nebezpečných e-mailových příloh. I když odesílatelé předstírají, že je v příloze faktura či dokumenty k platbě, ve skutečnosti se tam nachází spustitelný soubor s příponou .exe. Otevřením si tak méně obezřetní uživatelé nevědomky nainstalují po spuštění škodlivý kód.

Tím vůbec nejrozšířenějším škodlivým kódem uplynulém v měsíci je Agent Tesla. „Největší útok spywaru Agent Tesla proběhl v České republice 3. října. Cílem útočníků, kteří tento škodlivý kód ve svých útočných kampaních využívají, jsou uživatelská hesla. Zranitelná a snadno dostupná jsou pak především hesla uložená v internetových prohlížečích,“ konstatoval Martin Jirkal, vedoucí analytického týmu v pražské pobočce Esetu. Na spyware Agent Tesla mohli uživatelé v říjnu narazit především v e-mailu s česky pojmenovanou přílohou Zpusob_platby, jpg.exe.

Při hraní klikl na reklamu. Pak přišel o 800 tisíc
Hry a systémy

„Počet detekcí spywaru Agent Tesla se i další měsíc drží stabilně na zhruba pětině všech zachycených případů pro platformu Windows v Česku. U říjnových útoků jsme mohli tentokrát pozorovat strategii, ve které se útočníci zaměřili na země ve střední a jihovýchodní části Evropy a na Turecko. Česká republika byla v říjnu čtvrtou nejvíce zasaženou zemí,“ konstatoval Jirkal.

Fotovoltaika funguje v zimě, instalace panelů trvá den

Zhruba každý desátý zachycený škodlivý kód byl password stealer Fareit.