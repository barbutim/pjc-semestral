#include <chrono>
#include <iostream>
#include "kmp.h"
#include "find.h"
#include "tools.h"

using namespace std;

int main(int argc, char* argv[]) {
    /** input and validation **/
    validateArguments(argc);

    string text = fileLoaderToString(argv[1]);
    string pattern = fileLoaderToString(argv[2]);

    validateFiles(text, pattern);

    /** KMP and Find functions **/
    auto startKMP = chrono::system_clock::now();
    size_t indexKMP = getKnuthMorrisPratt(text, pattern, getPartialMatchTable(pattern));
    auto stopKMP = chrono::system_clock::now();
    auto durationKMP = chrono::duration_cast<chrono::nanoseconds>(stopKMP - startKMP).count();

    auto startFind = chrono::system_clock::now();
    size_t indexFind = find(text, pattern);
    auto stopFind = chrono::system_clock::now();
    auto durationFind = chrono::duration_cast<chrono::nanoseconds>(stopFind - startFind).count();

    /** results **/
    printPartialMatchTable(getPartialMatchTable(pattern));
    printKnuthMorrisPratt(indexKMP);
    printFind(indexFind);

    cout << "--- Timers ---" << endl;
    cout << "Knuth Morris Pratt: " << durationKMP << " ns" << endl;
    cout << "Find: " << durationFind << " ns" << endl;

    return 0;
}
