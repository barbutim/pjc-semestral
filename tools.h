#ifndef KMP_TOOLS_H
#define KMP_TOOLS_H

using namespace std;

string fileLoaderToString(const string& fileName);
void validateArguments(int size);
void validateFiles(const string& text, const string& pattern);

#endif