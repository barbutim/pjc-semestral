#include <string>
#include <fstream>
#include <iostream>
#include "tools.h"

using namespace std;

string fileLoaderToString(const string& fileName) {
    ifstream file;
    file.open("../examples/" + fileName);
    string fileData;
    if(file.is_open()){
        string element;
        while(getline(file, element)){
            fileData.append(element);
        }
        file.close();
    }
    else {
        throw invalid_argument("ERROR: Could not read from " + fileName + " file!");
    }
    return fileData;
}

void validateArguments(int size) {
    if(size != 3) {
        throw invalid_argument("ERROR: You have to pass exactly 2 arguments (first: text, second: pattern).");
    }
}

void validateFiles(const string& text, const string& pattern) {
    if(text.length() == 0) {
        throw invalid_argument("ERROR: The selected 'text' file must have at least one character!");
    }
    if(pattern.length() == 0) {
        throw invalid_argument("ERROR: The selected 'pattern' file must have at least one character!");
    }
}