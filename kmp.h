#ifndef KMP_KMP_H
#define KMP_KMP_H

#include <vector>
#include <string>

using namespace std;

void printPartialMatchTable(const vector<int>& PMTable);
vector<int> getPartialMatchTable(string pattern);

void printKnuthMorrisPratt(size_t matchIndex);
size_t getKnuthMorrisPratt(string text, string pattern, vector<int> PMTable);

#endif