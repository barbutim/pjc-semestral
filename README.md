# Semestrálna práca PJC



## Popis zadania

Když se pokusíme o vyhledání dlouhého řetězce (10000 a víc znaků) v dlouhém řetězce pomocí knihovní funkce std::find(), operace chvilku trvá. Proces lze urychlit využitím vhodného algoritmu. V roce 1976 byl představen [Knuth-Morris-Pratt algoritmus](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm), který díky vhodnému předzpracování dokáže proces vyhledání podřetězců urychlit.

## Popis implementácie

Implementácia aplikácie spočívala vo vytvorení vyššie uvedeného algoritmu. Pre porovnanie je zavolaná aj štandardná funkcia std::find(). 

Rozdelenie tried:

- kmp.cpp / kmp.h: predstavuje implementáciu samotného KMP algoritmu vrátane Failure Function, ktorú táto funkcia využíva
- find.cpp / find.h: predstavuje implementáciu funkcie std::find()
- tools.cpp / tools.h: predstavuje validátory a načítanie súborov z inputu
- main.cpp: predstavuje hlavnú funkciu programu, odkiaľ sa volajú ostatné funkcie

## Popis funkčnosti a ovládania aplikácie

Aplikácia očakáva dva vstupné argumenty. Oba argumenty musia byť názvy súborov, ktoré program rozbalí a následne z nich bude čítať.

Prvým argumentom je samotný text, a druhým argumentom je pattern, ktorý sa bude v danom texte vyhľadávať.
Pri zadaní zlých vstupov alebo prázdnych súborov je vyhodená chyba.

## Výsledky behu programu

Počas testovania som skúšal rôzne vstupy a patterny, pričom som prišiel k nasledujúcim záverom:

Pri bežných vstupoch ako sú texty, články a podobne, nie je tento algoritmus vhodný. Dôvodom je, že sa v bežných slovách neopakujú rovnaké sady písmen/slóv. Znamená to že Partial Match Table / Failure Function vytvorí vektor, ktorý má ale väčšinu hodnôt rovných = 0 respektíve tieto hodnoty nie sú veľmi vysoké.

Pri upravených vstupoch, ktoré sú zpravidla dlhé a zároveň sa v nich opakujú rovnaké písmena/slová, je táto situácia opačná. Partial Match Table / Failure Function vytvorí vektor, ktorý obsahuje vysoké hodnoty. Prakticky sa dá povedať, čím vyššie hodnoty sú v tomto vektore, tým je efektívnosť tohto algoritmu vyššia.

##### Testovanie: 

text1.txt - pattern1.txt: krátky text, opakujúce sa slová (250 znakov)

    Priemerné hodnoty:
    Knuth Morris Pratt: 22900 ns
    Find: 2300 ns

text2.txt - pattern2.txt: krátky text, NEopakujúce sa slová (250 znakov)

    Priemerné hodnoty:
    Knuth Morris Pratt: 21600 ns
    Find: 1100 ns

_Pri krátkych textoch je vždy suverénne rýchlejší std::find() algoritmus. Pri opakujúcich sa slovách síce vidíme dlhší response, avšak nárast je viac-menej zanedbateľný._

text3.txt - pattern3.txt: dlhý text, NEopakujúce sa písmená, články z Novinky.cz (15 000 znakov)

    Priemerné hodnoty:
    Knuth Morris Pratt: 344100 ns
    Find: 26500 ns

_Pri dlhých textoch, v ktorých sa ale vyskytujú normálne slová, je stále rýchlejší algoritmus std::find(). I keď sa jedná o dlhý text, čo by mohlo napovedať vyššej efektívnosti u KMP algoritmu, opak je pravdou. Vektor pre Partial Match Table / Failure Function je v tomto prípade väčšinou rovný = 0. Je to spôsobené samozrejme tým, že v článkoch sa rovnaké písmená/slová bežne nevyskytujú, čo znamená VÝRAZNE vyššiu efektívnosť pre std::find() algoritmus_

text4.txt - pattern4.txt: dlhý text, NEopakujúce sa písmená, generátor písmen (15 000 znakov)

    Priemerné hodnoty:
    Knuth Morris Pratt: 294500 ns
    Find: 19200 ns

_Opäť tu máme rovnako dlhý text ako v predchádzajúcom teste, tentokrát bol ale text náhodne vygenerovaný. Znamená to, že neobsahuje žiadne spisovné slová. Vo výsledku sa ale vektor pre Partial Match Table / Failure Function príliš nelíši od toho predchádzajúceho, väčšina hodnôt je rovná = 0. Za všimnutie ale stojí, že response sa v tomto prípade znížil pri oboch algoritmoch o približne -20%._

text5.txt - pattern5.txt: dlhý text, opakujúce sa písmená (15 000 znakov)

    Priemerné hodnoty:
    Knuth Morris Pratt: 829100 ns
    Find: 952900 ns

_V poslednom prípade sa jedná opäť o dlhý text, avšak väčšina textu sú rovnaké alebo veľmi podobné písmená. Vektor pre Partial Match Table / Failure Function teda obsahuje hodnoty vysoko nad nulou. V tomto prípade je teda už KMP algoritmus značne rýchlejší. Pri ďalšom pokračovaní napr. pre 30 000 znakov rovnakého typu sa tento rozdiel už bude len zväčšovať v prospech KMP algoritmu._

text6.txt - pattern6.txt: prázdne súbory pre testovanie

    Priemerné hodnoty:
    Knuth Morris Pratt: -
    Find: -


## Záver

KMP algoritmus má určite svoju hlavnú výhodu, ktorou je zaručená worst-case efektivita. V týchto prípadoch je značne rýchlejší ako klasický algoritmus std::find(). Na druhú stranu, pri bežnom vyhľadávaní podreťazcov bude väčšinou rýchlejší práve tento std::find() algoritmus, tak ako už som to popisoval vyššie.

Scenáre, v ktorých je KMP algoritmus rýchlejší sú teda nie až tak bežné a teda pochopiteľne je preferovaný druhý algoritmus. Určite to bola ale dobrá skúsenosť, hlavne pri testovaní reťazcov a hľadaní vhodných kombinácií, kvôli testom. :)
