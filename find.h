#ifndef KMP_FIND_H
#define KMP_FIND_H

using namespace std;

void printFind(size_t matchIndex);
size_t find(const string& text, const string& pattern);

#endif